package main

import (
	"log"
	"tgcensus/config"
)

func main() {
	err := config.Load()
	if err != nil {
		panic(err)
	}

	log.Println(config.Env.AppId)
}
