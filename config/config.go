package config

import (
	"os"
	"strconv"

	"github.com/joho/godotenv"
)

type env struct {
	AppId   int64
	AppHash string
}

var Env env

func Load() error {
	err := godotenv.Load(".env")
	if err != nil {
		return err
	}

	rawAppId, err := strconv.Atoi(os.Getenv("TELEGRAM_APP_ID"))
	if err != nil {
		return err
	}

	Env = env{
		AppId:   int64(rawAppId),
		AppHash: os.Getenv("TELEGRAM_APP_HASH"),
	}

	return nil
}
